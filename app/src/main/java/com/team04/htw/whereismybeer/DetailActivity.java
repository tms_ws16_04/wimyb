package com.team04.htw.whereismybeer;

/**
 * Created by Maximilian Stange and Johann Winter
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import osm.PersistenceManager;
import osm.Pub;

public class DetailActivity extends Activity {

    private Pub detailedPub;
    private PersistenceManager persistenceManager;
    private ImageButton favoriteButton;
    private ArrayList<Integer> idList;
    private static final int NUMBER_OF_CHARACTERS_HOUSE_NUMBER = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        favoriteButton = (ImageButton) findViewById(R.id.favoriteButton);
        Intent intent = getIntent();

        if (intent.hasExtra("callingActivity") && intent.getStringExtra("callingActivity").equals("FavoritesActivity"))
            detailedPub = (Pub) intent.getExtras().getSerializable("detailedPub");

        if (intent.hasExtra("callingActivity") && intent.getStringExtra("callingActivity").equals("MapActivity"))
            detailedPub = (Pub) intent.getExtras().getSerializable("detailedPub");

        persistenceManager = new PersistenceManager(this);
        ArrayList<Pub> favoritePubs = persistenceManager.loadFavorites();
        idList = new ArrayList<>();
        for (Pub p : favoritePubs) {
            idList.add(p.getPlaceID());
        }

        if (idList.contains(detailedPub.getPlaceID())) {
            favoriteButton.setImageResource(R.drawable.heart_btn_filled);
        } else {
            favoriteButton.setImageResource(R.drawable.heart_btn_unfilled);
        }

        TextView nameTextView = (TextView) findViewById(R.id.nameTextView);
        TextView cityTextView = (TextView) findViewById(R.id.cityTextView);
        TextView addressTextView = (TextView) findViewById(R.id.addressTextView);

        nameTextView.setText(detailedPub.getNameInformation()[0]);

        // sort street name and house number
        if (detailedPub.getNameInformation()[2].length() < detailedPub.getNameInformation()[1].length()) {
            addressTextView.setText(detailedPub.getNameInformation()[1] + ", " + detailedPub.getNameInformation()[2]);
        } else {
            addressTextView.setText(detailedPub.getNameInformation()[2] + ", " + detailedPub.getNameInformation()[1]);
        }
        cityTextView.setText(detailedPub.getNameInformation()[3]+", "+detailedPub.getNameInformation()[4]+"\n"+detailedPub.getNameInformation()[5]);
    }

    public void onMapClick(View view) {
        Intent gotoMapActivityIntent = new Intent(this, MapActivity.class);
        gotoMapActivityIntent.putExtra("detailedPub", detailedPub);
        gotoMapActivityIntent.putExtra("callingActivity", "DetailActivity");
        final int result = 1;
        startActivityForResult(gotoMapActivityIntent, result);
    }


    public void onSaveToFavoritesClick(View view) {


        ArrayList<Pub> favoritePubs = persistenceManager.loadFavorites();
        idList.clear();
        for (Pub p : favoritePubs) {
            idList.add(p.getPlaceID());
        }
        if (idList.contains(detailedPub.getPlaceID())) {
            favoriteButton.setImageResource(R.drawable.heart_btn_unfilled);
            if (getPubIndex(favoritePubs, detailedPub) > -1) {
                favoritePubs.remove(getPubIndex(favoritePubs, detailedPub));

                CharSequence text = getResources().getString(R.string.removed_from_favorites);
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(this, text, duration);
                toast.show();
            }
        } else {
            favoriteButton.setImageResource(R.drawable.heart_btn_filled);
            favoritePubs.add(detailedPub);

            CharSequence text = getResources().getString(R.string.saved_to_favorites);
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
        }
        persistenceManager.saveFavorites(favoritePubs);
    }

    private int getPubIndex(List<Pub> list, Pub pub) {
        int index = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getPlaceID() == pub.getPlaceID())
                index = i;
        }
        return index;
    }

}



