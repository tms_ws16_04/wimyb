package com.team04.htw.whereismybeer;

/**
 * Created by Maximilian Stange and Johann Winter
 */

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import org.json.JSONException;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import osm.connection.OSMLocationFinder;
import ui.OnItemClickListener;
import osm.parser.OSMParser;
import osm.connection.AsyncResponse;
import osm.connection.Parameter.LocationType;
import osm.overlays.LocationPositionOverlay;
import osm.overlays.UserPositionOverlay;
import osm.PersistenceManager;
import osm.Pub;
import ui.RecyclerAdapter;
import ui.states.ListAppearedState;
import ui.states.ListDeselectedState;
import ui.states.ListNotDisplayed;
import ui.states.ListSelectedState;
import ui.states.ListState;

public class MapActivity extends Activity implements AsyncResponse, OnItemClickListener {

    private static final int PERMISSION_ALL = 1;
    private final String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    private final DisplayMetrics metrics = new DisplayMetrics();
    private Timer t;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location location;
    private UserPositionOverlay userPositionOverlay;
    private MapView map;
    private IMapController mapController;
    private FloatingActionButton fab;
    private OSMLocationFinder locationFinder;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private ListState listState;
    private Pub focusOnPub;
    private PersistenceManager persistenceManager;
    private ArrayList<Pub> results = new ArrayList<>();
    private static final int ZOOMFACTOR_CENTER_LOCATION = 19;
    private static final int ZOOMFACTOR_SHOW_LOCATIONS = 15;
    private static final int SCHEDULE_PERIOD = 1500;
    private static final int CENTER_LOCATION_DELAY = 250;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            setContentView(R.layout.emptymapactivity);
            createTimerTaskForMapLoading(this);
        } else {
            setContentView(R.layout.mapactivity);
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            fab = (FloatingActionButton) findViewById(R.id.fab);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            persistenceManager = new PersistenceManager(this);
            createList();
            loadMap();
            Drawable userMarker = getResources().getDrawable(R.drawable.icon_pin_user);
            userPositionOverlay = new UserPositionOverlay(userMarker);
            map.getOverlays().add(userPositionOverlay);

            // Add compass
            CompassOverlay compassOverlay = new CompassOverlay(this, new InternalCompassOrientationProvider(this), map);
            compassOverlay.enableCompass();
            map.getOverlays().add(compassOverlay);

            // Rotation gestures - but deprecated
            RotationGestureOverlay rotationGestureOverlay = new RotationGestureOverlay(this, map);
            rotationGestureOverlay.setEnabled(true);
            map.setMultiTouchControls(true);
            map.getOverlays().add(rotationGestureOverlay);

            try {
                if (locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
                    centerUserLocation(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
                }
                if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null && locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) == null) {
                    centerUserLocation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            }
            registerGestureListener();
            registerLocationListener();


            Intent intent = getIntent();
            if (intent.hasExtra("callingActivity") && intent.getStringExtra("callingActivity").equals("DetailActivity")) {
                focusOnPub = (Pub) intent.getExtras().getSerializable("detailedPub");
                Location l = focusOnPub.getLocation();

                ArrayList<Pub> searchResults = persistenceManager.loadSearchResults();
                if (searchResults != null) {
                    results = searchResults;
                    setClickableAdapter(true);
                    showLocationsMap(results);
                    listState.changePosition(this);
                    centerLocation(l);
                }
            }
        }
    }

    private void registerLocationListener() {
        locationListener = new LocationListener() {
            public void onLocationChanged(Location loc) {
                // Called when a new location is found by the network location provider.
                if (location == null) {
                    centerUserLocation(loc);
                }
                setLocation(loc);
            }
            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };
    }

    private void showDetailActivity(Pub p) {
        Intent gotoDetailActivityIntent = new Intent(this, DetailActivity.class);
        gotoDetailActivityIntent.putExtra("detailedPub", p);
        gotoDetailActivityIntent.putExtra("callingActivity", "MapActivity");
        final int result = 1;
        startActivityForResult(gotoDetailActivityIntent, result);
    }

    private void centerUserLocation(Location location) {
        GeoPoint point = new GeoPoint(location.getLatitude(), location.getLongitude());
        mapController.animateTo(point);
        mapController.setZoom(ZOOMFACTOR_CENTER_LOCATION);
        userPositionOverlay.clear();
        userPositionOverlay.addItem(point, "UserPosition", "Position");
    }

    private void centerLocation(Location location) {
        GeoPoint point = new GeoPoint(location.getLatitude(), location.getLongitude());
        mapController.setZoom(ZOOMFACTOR_CENTER_LOCATION);
        mapController.animateTo(point);
    }

    private void createList() {
        listState = new ListNotDisplayed();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setScrollableLayoutManager(false);
        recyclerView.setBackgroundResource(R.drawable.shape);
        recyclerView.setAlpha(0.9f);
        recyclerView.setLayoutParams(new AppBarLayout.LayoutParams((int) (metrics.widthPixels * 0.96), (int) (metrics.heightPixels * 0.8)));
        recyclerView.setY(metrics.heightPixels);
    }

    private void registerGestureListener() {
        map.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (listState instanceof ListSelectedState)
                    listState.changePosition(MapActivity.this);
                System.out.println(view.getClass());
                return false;
            }
        });
    }

    private Drawable createLocationPositionMarker() {
        Drawable marker = getResources().getDrawable(R.drawable.icon_pin_beer_72p);
        return marker;
    }

    @Override
    protected void onStart() {
        try {
            if (hasPermissions(this, PERMISSIONS)) {
                // Register the listener with the Location Manager to receive location updates
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1000, locationListener);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1000, locationListener);
            }
            super.onStart();
        } catch (SecurityException e) {
            e.printStackTrace();
        }

    }


    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String p : permissions) {
                if (ActivityCompat.checkSelfPermission(context, p) != PackageManager.PERMISSION_GRANTED)
                    return false;
            }
        }
        return true;
    }

    private void loadMap() {
        //important! set your user agent to prevent getting banned from the osm servers
        org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants.setUserAgentValue(BuildConfig.APPLICATION_ID);
        map = (MapView) findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        mapController = map.getController();
    }

    /**
     *
     * Creates a timer which checks every 5 seconds if the map is ready for load
     * If the map is ready due to having permissions, the timer HAS TO create a new thread which runs on main thread to change activity content
     */
    private void createTimerTaskForMapLoading(final Context context) {
        t = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (hasPermissions(context, PERMISSIONS)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(MapActivity.this, MapActivity.class));

                        }
                    });
                    this.cancel();
                    t = null;
                }
            }
        };
        t.scheduleAtFixedRate(task, 0, SCHEDULE_PERIOD);
    }

    /**
     * onClick Function if permissions have been denied to request permission again
     */
    public void requestPermissions(View view) {
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }

    public void onFabButtonClick(View view) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        locationFinder = new OSMLocationFinder(this, this, progressDialog, null);
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() && location != null) {
            locationFinder.startSearching(location.getLongitude() , location.getLatitude(), LocationType.PUB);
        } else {
            Snackbar.make(view, getResources().getString(R.string.determining_position), Snackbar.LENGTH_LONG).setActionTextColor(Color.RED).show();
        }
    }

    @Override
    public void searchFinish(String output) {
        try {
            results = OSMParser.parseToPub(output);
            persistenceManager.saveSearchResults(new ArrayList<>(results));
            setClickableAdapter(true);
            showLocationsMap(results);
            listState.changePosition(this);

        } catch (JSONException e) {
            e.printStackTrace();
            CharSequence text = getResources().getString(R.string.error_json_conversion);
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
        }
    }

    private void showLocationsMap(List<Pub> list) {
        Drawable locationMarker = createLocationPositionMarker();
        LocationPositionOverlay locationPositionOverlay = new LocationPositionOverlay(locationMarker);
        for (Pub p : list) {
            GeoPoint point = new GeoPoint(p.getLatitude(), p.getLongitude());
            System.out.println(p.getLatitude() + "   " + p.getLongitude());
            locationPositionOverlay.addItem(point, p.getNameInformation()[0], String.valueOf(p.getPlaceID()));
        }
        map.getOverlays().add(locationPositionOverlay);
        mapController.setZoom(ZOOMFACTOR_SHOW_LOCATIONS);
    }

    @Override
    public void geoCodingFinish(String output) {}


    public void onFavoriteButtonClick(View view) {
        Intent gotoFavoritesActivityIntent = new Intent(this, FavoritesActivity.class);
        final int result = 1;
        startActivityForResult(gotoFavoritesActivityIntent, result);
    }

    @Override
    public void onItemClick(Pub currentPub, View v, int itemPosition) {
        if (listState instanceof ListAppearedState)
            listState.changePosition(this);
        else {
            if (v.getClass().equals(ImageButton.class))
                showDetailActivity(currentPub);
            else {
                listState.changePosition(this);
                focusOnPub = currentPub;
                recyclerView.postOnAnimationDelayed(new Runnable() {
                    @Override
                    public void run() {
                        centerLocation(focusOnPub.getLocation());
                    }
                }, CENTER_LOCATION_DELAY);
            }
        }
    }

    private void setClickableAdapter(boolean isClickable) {
        adapter = new RecyclerAdapter(results, this, isClickable);
        recyclerView.setAdapter(adapter);
    }

    public void setScrollableLayoutManager(final boolean isScrollable) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return isScrollable;
            }
        };
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    // back-key behavior
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (listState instanceof ListNotDisplayed) {
                finish();
            }
            if (listState instanceof ListAppearedState) {
                setListState(new ListDeselectedState());
                listState.changePosition(this);
            }
            if (listState instanceof ListSelectedState) {
                listState.changePosition(this);
            }
        }
        return true;
    }

    private void setLocation(Location loc) {
        location = loc;
    }

    public RecyclerView getRecyclerView() {
        return this.recyclerView;
    }

    public DisplayMetrics getMetrics() {
        return this.metrics;
    }

    public void setListState(ListState lS) {
        this.listState = lS;
    }

    public ListState getListState(){
        return listState;
    }
    public MapView getMap() {
         return this.map;
    }

    public FloatingActionButton getFab() {
        return  this.fab;
    }
}

