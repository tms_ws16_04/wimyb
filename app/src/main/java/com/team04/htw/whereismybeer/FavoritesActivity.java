package com.team04.htw.whereismybeer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.util.ArrayList;
import osm.PersistenceManager;
import osm.Pub;
import ui.favorites.FavoritesOnItemClickListener;
import ui.favorites.FavoritesRecyclerAdapter;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class FavoritesActivity extends Activity implements FavoritesOnItemClickListener {

    private RecyclerView favoritesListView;
    private PersistenceManager persistenceManager;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorites_activity);
        favoritesListView = (RecyclerView) findViewById(R.id.favoritesListview);
        persistenceManager = new PersistenceManager(this);

        loadFavorites();
    }

    @Override
    public void onItemClick(Pub p, View v, int itemPosition) {
        Intent gotoDetailActivityIntent = new Intent(this, DetailActivity.class);
        gotoDetailActivityIntent.putExtra("callingActivity", "FavoritesActivity");
        gotoDetailActivityIntent.putExtra("detailedPub", p);
        final int result = 1;
        startActivityForResult(gotoDetailActivityIntent, result);
    }


    @Override
    protected void onResume() {
        super.onResume();
        loadFavorites();
    }


    private void loadFavorites() {
        ArrayList<Pub> savedPubs = persistenceManager.loadFavorites();
        FavoritesRecyclerAdapter favorites_recyclerAdapter = new FavoritesRecyclerAdapter(savedPubs, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        favoritesListView.setLayoutManager(linearLayoutManager);
        favoritesListView.setAdapter(favorites_recyclerAdapter);
    }
}