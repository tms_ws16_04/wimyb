package osm.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import osm.Pub;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class OSMParser {

    public static ArrayList<Pub> parseToPub(String json) throws JSONException {
        System.out.println(json);
        JSONArray array = new JSONArray(json);
        ArrayList<Pub> pubList = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            int placeId = array.getJSONObject(i).getInt("place_id");
            int osmId = array.getJSONObject(i).getInt("osm_id");
            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
            double latitude = array.getJSONObject(i).getDouble("lat");
            double longitude = array.getJSONObject(i).getDouble("lon");
            String[] nameInfo = array.getJSONObject(i).getString("display_name").split(",");
            pubList.add(new Pub(placeId, osmId, latitude, longitude, nameInfo));
        }
        return pubList;
    }

    public static String parseToPositionAsString(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);
        return obj.getString("display_name");
    }
}
