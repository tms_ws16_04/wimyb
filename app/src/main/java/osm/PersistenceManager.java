package osm;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.io.File;
import java.util.HashSet;


/**
 * Created by Maximilian Stange and Johann Winter
 */

public class PersistenceManager {

    private final Context context;
    private static final String RESULTS_FILENAME = "savedResults";
    private static final String FAVORITES_FILENAME = "savedFavorites";


    public PersistenceManager(Context ctx) {
        context = ctx;
    }


    public ArrayList<Pub> loadSearchResults() {
        FileInputStream fileInStream = null;
        ObjectInputStream ois = null;
        ArrayList<Pub> searchResults = new ArrayList<>();

        try {
            fileInStream = context.openFileInput(RESULTS_FILENAME);
            ois = new ObjectInputStream(fileInStream);

            ArrayList<Pub> pubs = (ArrayList<Pub>) ois.readObject();
            searchResults.addAll(pubs);
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return searchResults;
    }


    public void saveSearchResults(ArrayList<Pub> searchResults) {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        new File(context.getFilesDir(), RESULTS_FILENAME);

        try {
            fos = context.openFileOutput(RESULTS_FILENAME, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(searchResults);
            System.out.println("Save successful");
        } catch (Exception e) {
            System.out.println("Save failed");
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e2) {
                    System.out.println("osw.close failed");
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e2) {
                    System.out.println("fos.close failed");
                }
            }
        }
    }


    /*
    public void deleteSearchResults() {
        File file = new File(context.getFilesDir(), RESULTS_FILENAME);
        file.delete();
    }

    public void deleteFavorites() {
        File file = new File(context.getFilesDir(), FAVORITES_FILENAME);
        file.delete();
    }
    */

    public ArrayList<Pub> loadFavorites() {
        FileInputStream fileInStream = null;
        ObjectInputStream ois = null;
        ArrayList<Pub> favorites = new ArrayList<>();

        // check if file exists
        if ((new File(context.getFilesDir(), FAVORITES_FILENAME)).exists()) {

            try {
                fileInStream = context.openFileInput(FAVORITES_FILENAME);
                ois = new ObjectInputStream(fileInStream);

                HashSet<Pub> pubs = (HashSet<Pub>) ois.readObject();
                favorites.addAll(pubs);
                ois.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return favorites;
    }


    public void saveFavorites(ArrayList<Pub> myFavorites) {
        HashSet<Pub> set = new HashSet<>(myFavorites);
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = context.openFileOutput(FAVORITES_FILENAME, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(set);
            System.out.println("Save successful");
        } catch (Exception e) {
            System.out.println("Save failed");
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e2) {
                    System.out.println("osw.close failed");
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e2) {
                    System.out.println("fos.close failed");
                }
            }
        }
    }
}