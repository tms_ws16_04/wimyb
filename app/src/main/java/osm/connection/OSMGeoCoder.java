package osm.connection;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import com.team04.htw.whereismybeer.R;
import osm.connection.Parameter.Format;

/**
 * Created by Maximilian Stange and Johann Winter
 */

class OSMGeoCoder {

    private String addressAsString = "";
    private final GeoCodingTask geoCodingTask;
    private String format = "";
    private final String root;

    public OSMGeoCoder(Context context, AsyncResponse delegate, @Nullable ProgressDialog pD, @Nullable String dialogMessage) {
        this.root = context.getResources().getString(R.string.osm_reverseSearch);
        this.format = Format.JSON.getFormat();

            geoCodingTask = new GeoCodingTask(context, delegate, pD, dialogMessage);


    }

    public void startGeoCoding(double latitude, double longitude) {
        String lat = "lat=" + String.valueOf(latitude);
        String lon = "lon=" + String.valueOf(longitude);
        String[] strings = {this.root, this.format, lat, lon};
        geoCodingTask.execute(strings);
    }


}
