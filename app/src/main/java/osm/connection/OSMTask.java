package osm.connection;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Maximilian Stange and Johann Winter
 */

abstract class OSMTask extends AsyncTask<String, Void, String> {

    private URL uri;
    private String result = "";
    private static final String GET_METHOD = "GET";
    protected static final String POST_METHOD = "POST";
    private static final int READ_TIMEOUT = 15000;
    private static final int CONNECTION_TIMEOUT = 15000;
    AsyncResponse delegate = null;
    ProgressDialog progressDialog;
    String dialogMessage = "";
    private static final int CONNECTION_RESPONSE_CODE = 200;


    @Override
    protected String doInBackground(String... strings) {
        try {
            createURI(strings);
            connectToUrl(getUri());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void createURI(String... strings) throws MalformedURLException {
        String uriAsString = "";
        if (checkForParamIntroducer(strings[0]))
            uriAsString = strings[0];
        else
            uriAsString = strings[0] + "?";
        for (int i = 1; i < strings.length; i++) {
            if (i == 1) {
                if (checkForParamSplitter(strings[i]))
                    strings[i] = strings[i].substring(1);
            } else {
                if (!checkForParamSplitter(strings[i]))
                    strings[i] = "&" + strings[i];
            }
            uriAsString += strings[i];
        }
        System.out.println(uriAsString);
        uri = new URL(uriAsString);
    }

    void setDelegate(AsyncResponse del) {
        this.delegate = del;
    }

    private URL getUri() {
        return this.uri;
    }

    private boolean checkForParamIntroducer(String url) {
        return url.charAt(url.length() - 1) == '?';
    }

    private boolean checkForParamSplitter(String param) {
        return param.charAt(0) == '&';
    }

    private void connectToUrl(URL theUrl) throws IOException {
        HttpsURLConnection connection = null;
        InputStream is = null;
        try {
            connection = (HttpsURLConnection) theUrl.openConnection();
            connection.setRequestMethod(GET_METHOD);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.setRequestProperty("user-agent", "WhereIsMyBeer"); // Wichtig sonst Status Code 403!!!
            connection.connect();
            if (connection.getResponseCode() == CONNECTION_RESPONSE_CODE)
                is = connection.getInputStream();
            result = getResult(is);
        } catch (ProtocolException e) {
            e.printStackTrace();
        } finally {
            if (is != null)
                is.close();
            if (connection != null)
                connection.disconnect();
        }
    }


    private String getResult(InputStream is) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        is.close();
        System.out.println("Result " + result);
        return result;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (this.progressDialog != null) {
                this.progressDialog.setMessage(dialogMessage);
                this.progressDialog.show();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (this.progressDialog.isShowing() && progressDialog != null)
            this.progressDialog.dismiss();
    }
}
