package osm.connection;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import com.team04.htw.whereismybeer.R;

/**
 * Created by Maximilian Stange and Johann Winter
 */


public class SearchLocationTask extends OSMTask {

    public SearchLocationTask(Context context, AsyncResponse delegate, @Nullable ProgressDialog progressDialog, @Nullable String dialogMessage) {
        setDelegate(delegate);
        this.progressDialog = progressDialog;
        if (dialogMessage != null)
            this.dialogMessage = dialogMessage;
        else
            this.dialogMessage = context.getResources().getString(R.string.default_dialogMessage);
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        delegate.searchFinish(s);
    }

}
