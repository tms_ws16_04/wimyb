package osm.connection;

import android.app.ProgressDialog;
import android.content.Context;

import osm.connection.Parameter.LocationType;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class OSMClient {

    private final OSMGeoCoder geoCoder;
    private final OSMLocationFinder locationFinder;

    public OSMClient(Context context, AsyncResponse delegate, ProgressDialog pD, String message) {
            this.geoCoder = new OSMGeoCoder(context, delegate, pD, message);
            this.locationFinder = new OSMLocationFinder(context, delegate, pD, message);
        }

    public void startLocationSearch(double lat, double lon, LocationType type) {
        this.locationFinder.startSearching(lat, lon, type);
    }

    public void startGeoCoding(double lat, double lon) {
        this.geoCoder.startGeoCoding(lat, lon);
    }

}
