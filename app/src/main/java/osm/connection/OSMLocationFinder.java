package osm.connection;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import com.team04.htw.whereismybeer.R;
import osm.connection.Parameter.Format;
import osm.connection.Parameter.LocationType;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class OSMLocationFinder {

    private SearchLocationTask searchLocationTask;
    private String format = "";
    private final String root;

    public OSMLocationFinder(Context context, AsyncResponse delegate, @Nullable ProgressDialog pD, @Nullable String dialogMessage) {
        this.root = context.getResources().getString(R.string.osm_search);
        this.format = Format.JSON.getFormat();

            searchLocationTask = new SearchLocationTask(context, delegate, pD, dialogMessage);


    }

    public void startSearching(double lon, double lat, LocationType loc) {
        String viewBoxLeft  = String.valueOf(lon - 0.04);
        String viewBoxTop = String.valueOf(lat - 0.04);
        String viewBoxRight = String.valueOf(lon + 0.04);
        String viewBoxBot = String.valueOf(lat + 0.04);
        String type = loc.getType();
        String query = String.format("q=%s", type);
        String limit = "limit=25";
        String viewBox = String.format("viewbox=%s,%s,%s,%s", viewBoxLeft, viewBoxTop, viewBoxRight, viewBoxBot);
        String bound = "bounded=1";
        String[] strings = {this.root, this.format, query, limit, viewBox, bound};
        System.out.println("URL:" + this.root + this.format + query + limit + viewBox);
        searchLocationTask.execute(strings);
    }
}
