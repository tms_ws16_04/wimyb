package osm.connection.Parameter;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public enum LocationType {
    PUB("[pub]");

    private final String type;

    LocationType(String s) {
        type = s;
    }

    public String getType() {
        return type;
    }
}
