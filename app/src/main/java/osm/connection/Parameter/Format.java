package osm.connection.Parameter;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public enum Format {
    JSON("format=json"),
    XML("format=xml"),
    HTML("format=html");

    private final String format;

    Format(String format) {
        this.format = format;
    }

    public String getFormat() {
        return this.format;
    }
}
