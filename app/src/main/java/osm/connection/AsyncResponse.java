package osm.connection;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public interface AsyncResponse {

    void geoCodingFinish(String output);

    void searchFinish(String output);
}
