package osm.overlays;

import android.graphics.Point;
import android.graphics.drawable.Drawable;
import org.osmdroid.api.IMapView;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import java.util.ArrayList;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class UserPositionOverlay extends ItemizedOverlay<OverlayItem> {

    private final ArrayList<OverlayItem> itemList = new ArrayList<>();

    public UserPositionOverlay(Drawable d) {
        super(d);
        Drawable marker = d;
        populate();
    }

    public void addItem(GeoPoint point, String title, String snippet) {
        OverlayItem item = new OverlayItem(title, snippet, point);
        itemList.add(item);
        populate();
    }
    @Override
    protected OverlayItem createItem(int i) {
        return itemList.get(i);
    }

    @Override
    public int size() {
        return itemList.size();
    }

    @Override
    public boolean onSnapToItem(int i, int i1, Point point, IMapView iMapView) {
        return false;
    }

    public void clear() {
        itemList.clear();
    }

}
