package osm;

import android.location.Location;

import java.io.Serializable;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public abstract class OSMObject implements Serializable {

    private int placeID;
    private int osmID;
    private double latitude;
    private double longitude;
    private String[] nameInformation;

    public int getPlaceID() {
        return placeID;
    }

    void setPlaceID(int placeID) {
        this.placeID = placeID;
    }

    public double getLatitude() {
        return latitude;
    }

    void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getOsmID() {
        return osmID;
    }

    void setOsmID(int osmID) {
        this.osmID = osmID;
    }

    public double getLongitude() {
        return longitude;
    }

    void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String[] getNameInformation() {
        return nameInformation;
    }

    void setNameInformation(String[] nameInformation) {
        this.nameInformation = nameInformation;
    }

    public Location getLocation() {
        Location l = new Location(String.valueOf(placeID));
        l.setLongitude(getLongitude());
        l.setLatitude(getLatitude());
        return l;
    }
}
