package osm;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class Pub extends OSMObject {

    public Pub(int pID, int osmID, double lat, double lon, String[] nameInformation) {
        setPlaceID(pID);
        setOsmID(osmID);
        setLatitude(lat);
        setLongitude(lon);
        setNameInformation(nameInformation);
    }
}
