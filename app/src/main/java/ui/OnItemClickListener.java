package ui;

import android.view.View;
import osm.Pub;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public interface OnItemClickListener {

    void onItemClick(Pub p, View v, int itemPosition);
}
