package ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.team04.htw.whereismybeer.R;
import osm.Pub;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class PubHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final OnItemClickListener onItemClickListener;
    private final TextView pubName;
    private final ImageButton infoBtn;
    private Pub currentPub;

    public PubHolder(View itemView, OnItemClickListener l) {
        super(itemView);
        onItemClickListener = l;
        pubName = (TextView) itemView.findViewById(R.id.pubName);
        infoBtn = (ImageButton) itemView.findViewById(R.id.infoIcon);
        itemView.setOnClickListener(this);
        infoBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.equals(this.itemView))
            onItemClickListener.onItemClick(currentPub, view, getPosition());
        if (view.equals(infoBtn))
            onItemClickListener.onItemClick(currentPub, view, getPosition());
    }

    public void bindPub(Pub pub) {
        currentPub = pub;
        pubName.setText(currentPub.getNameInformation()[0]);
    }
}
