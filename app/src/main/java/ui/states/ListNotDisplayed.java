package ui.states;

import android.app.Activity;
import android.view.View;
import com.team04.htw.whereismybeer.MapActivity;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class ListNotDisplayed implements ListState {

    private static final int ANIMATION_TRANSLATION_DURATION = 1500;
    private static final int ANIMATION_ALPHA_DURATION = 1200;


    @Override
    public void changePosition(Activity a) {
        final MapActivity mA = ((MapActivity) a);
        mA.getRecyclerView().animate().translationY((int) (mA.getMetrics().heightPixels * 0.8)).setDuration(ANIMATION_TRANSLATION_DURATION);
        mA.setListState(new ListAppearedState());

        // remove FloatingActionButton
        mA.getFab().animate().alpha(0).setDuration(ANIMATION_ALPHA_DURATION).withEndAction(new Runnable() {
            @Override
            public void run() {
                mA.getFab().setVisibility(View.INVISIBLE);
            }
        });

    }
}
