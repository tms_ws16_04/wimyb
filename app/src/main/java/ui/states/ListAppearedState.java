package ui.states;

import android.app.Activity;
import com.team04.htw.whereismybeer.MapActivity;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class ListAppearedState implements ListState {

    private static final int ANIMATION_TRANSLATION_DURATION = 750;
    private static final int ANIMATION_ALPHA_DURATION = 750;
    private static final float ALPHA_FIFTY_PERCENT = 0.5f;

    @Override
    public void changePosition(Activity a) {
        MapActivity mA = ((MapActivity) a);
        mA.getRecyclerView().setOnTouchListener(null);
        mA.setScrollableLayoutManager(true);
        mA.getRecyclerView().animate().translationY((int) (mA.getMetrics().heightPixels * 0.16)).setDuration(ANIMATION_TRANSLATION_DURATION);
        mA.setListState(new ListSelectedState());
        mA.getMap().animate().alpha(ALPHA_FIFTY_PERCENT).setDuration(ANIMATION_ALPHA_DURATION);
    }
}
