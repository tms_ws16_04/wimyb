package ui.states;

import android.app.Activity;
import android.view.View;

import com.team04.htw.whereismybeer.MapActivity;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class ListDeselectedState implements ListState {

    private static final int ANIMATION_TRANSLATION_DURATION = 400;
    private static final int ANIMATION_ALPHA_DURATION = 300;

    @Override
    public void changePosition(Activity a) {
        final MapActivity mA = ((MapActivity) a);
        mA.getRecyclerView().animate().translationY(mA.getMetrics().heightPixels).setDuration(ANIMATION_TRANSLATION_DURATION);
        mA.getFab().animate().alpha(1).setDuration(ANIMATION_ALPHA_DURATION).withStartAction(new Runnable() {
            @Override
            public void run() {
                mA.getFab().setVisibility(View.VISIBLE);
            }
        });
        mA.setListState(new ListNotDisplayed());
    }
}
