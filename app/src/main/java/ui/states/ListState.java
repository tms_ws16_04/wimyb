package ui.states;

import android.app.Activity;


/**
 * Created by Maximilian Stange and Johann Winter
 */

public interface ListState {
    void changePosition(Activity activity);
}
