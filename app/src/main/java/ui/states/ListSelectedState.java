package ui.states;

import android.app.Activity;
import com.team04.htw.whereismybeer.MapActivity;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class ListSelectedState implements ListState {

    private static final int ANIMATION_ALPHA_DURATION = 300;


    @Override
    public void changePosition(Activity a) {
        MapActivity mA = ((MapActivity) a);
        mA.setScrollableLayoutManager(false);
        mA.getRecyclerView().animate().translationY((int) (mA.getMetrics().heightPixels * 0.8)).setDuration(400);
        mA.setListState(new ListAppearedState());
        mA.getMap().animate().alpha(1.0f).setDuration(ANIMATION_ALPHA_DURATION);
    }
}
