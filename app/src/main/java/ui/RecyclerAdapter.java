package ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.team04.htw.whereismybeer.R;
import java.util.ArrayList;
import osm.Pub;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class RecyclerAdapter extends RecyclerView.Adapter<PubHolder> {

    private final ArrayList<Pub> list;
    private final OnItemClickListener listener;
    private boolean isEnabled = false;

    public RecyclerAdapter(ArrayList<Pub> arrayList, OnItemClickListener listener, boolean isEnabled) {
        this.listener = listener;
        list = arrayList;
        this.isEnabled = isEnabled;
    }

    @Override
    public PubHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mapactivity_listview, parent, false);
        inflatedView.setEnabled(isEnabled);
        return new PubHolder(inflatedView, listener);
    }

    @Override
    public void onBindViewHolder(PubHolder holder, int position) {
        Pub p = list.get(position);
        holder.bindPub(p);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setChildrenEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
}
