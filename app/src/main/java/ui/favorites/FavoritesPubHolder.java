package ui.favorites;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.team04.htw.whereismybeer.R;
import osm.Pub;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class FavoritesPubHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final FavoritesOnItemClickListener favoritesOnItemClickListener;
    private final TextView pubName;
    private Pub currentPub;

    public FavoritesPubHolder(View itemView, FavoritesOnItemClickListener l) {
        super(itemView);
        favoritesOnItemClickListener = l;
        pubName = (TextView) itemView.findViewById(R.id.name);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.equals(this.itemView))
            favoritesOnItemClickListener.onItemClick(currentPub, view, getPosition());
    }

    public void bindPub(Pub pub) {
        currentPub = pub;
        pubName.setText(currentPub.getNameInformation()[0]);
    }
}
