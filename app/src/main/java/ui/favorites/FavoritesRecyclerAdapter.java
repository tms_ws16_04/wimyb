package ui.favorites;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.team04.htw.whereismybeer.R;
import java.util.ArrayList;
import osm.Pub;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public class FavoritesRecyclerAdapter extends RecyclerView.Adapter<FavoritesPubHolder> {

    private final ArrayList<Pub> list;
    private final FavoritesOnItemClickListener listener;

    public FavoritesRecyclerAdapter(ArrayList<Pub> arrayList, FavoritesOnItemClickListener listener) {
        this.listener = listener;
        list = arrayList;
    }

    @Override
    public FavoritesPubHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorites_listview_row, parent, false);
        return new FavoritesPubHolder(inflatedView, listener);
    }

    @Override
    public void onBindViewHolder(FavoritesPubHolder holder, int position) {
        Pub p = list.get(position);
        holder.bindPub(p);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
