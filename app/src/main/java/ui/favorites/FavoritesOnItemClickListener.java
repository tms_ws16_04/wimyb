package ui.favorites;

import android.view.View;
import osm.Pub;

/**
 * Created by Maximilian Stange and Johann Winter
 */

public interface FavoritesOnItemClickListener {

    void onItemClick(Pub p, View v, int itemPosition);
}
