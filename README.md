# WhereIsMyBeer



Die Applikation "Where is my beer" ist für diejenigen, die gerne die kleinen und großen, bekannten und unbekannten, glamourösen und altmodischen Bars und Kneipen in der Umgebung kennen lernen möchten.


## Installation

* Einfach die APK installieren bzw. im Emulator starten.


## Todos (next version)

* Testing
* Fragments
* Navigation


## Autor

* Maximilian Stange, maximilianstange@yahoo.de
* Johann Winter, johann.w@live.de


## Version

* Version 0.7


## License

Free to use.


## Screenshots

* Kartenansicht

![rsz_screenshot_1486124642.png](https://bitbucket.org/repo/G5RG6L/images/3342825883-rsz_screenshot_1486124642.png)


* Listenansicht

![rsz_screenshot_1486124649.png](https://bitbucket.org/repo/G5RG6L/images/445447705-rsz_screenshot_1486124649.png)

* Detailansicht

![rsz_1screenshot_1486124789.png](https://bitbucket.org/repo/G5RG6L/images/348343753-rsz_1screenshot_1486124789.png)

* Favoritenansicht

![rsz_screenshot_1486124654.png](https://bitbucket.org/repo/G5RG6L/images/670478019-rsz_screenshot_1486124654.png)